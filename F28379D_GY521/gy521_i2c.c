
//
// Included Files
//
#include "driverlib.h"
#include "device.h"
#include "gy521.h"
#include "quad_i2c.h"

//
// Defines
//
#define SLAVE_ADDRESS               MPU6050_DEFAULT_ADDRESS


int16_t accX = 0;
int16_t accY = 0;
int16_t accZ = 0;
int16_t temp = 0;
float32_t temp_centigrad = 0;
int16_t gyrX = 0;
int16_t gyrY = 0;
int16_t gyrZ = 0;

//
// I2C message states for I2CMsg struct
//
#define MSG_STATUS_INACTIVE         0x0000 // Message not in use, do not send
#define MSG_STATUS_SEND_WITHSTOP    0x0010 // Send message with stop bit
#define MSG_STATUS_WRITE_BUSY       0x0011 // Message sent, wait for stop
#define MSG_STATUS_SEND_NOSTOP      0x0020 // Send message without stop bit
#define MSG_STATUS_SEND_NOSTOP_BUSY 0x0021 // Message sent, wait for ARDY
#define MSG_STATUS_RESTART          0x0022 // Ready to become master-receiver
#define MSG_STATUS_READ_BUSY        0x0023 // Wait for stop before reading data

//
// Error messages for read and write functions
//
#define ERROR_BUS_BUSY              0x1000
#define ERROR_STOP_NOT_READY        0x5555
#define ERROR_ENABLE_FAIL           0x1001
#define ERROR_DATAREAD_FAIL         0x1002
#define SUCCESS                     0x0000



//
// Globals
//
struct I2CMsg GY521_i2cMsgOut = {MSG_STATUS_SEND_WITHSTOP,
                           SLAVE_ADDRESS,
                           1,
                           MPU6050_RA_PWR_MGMT_1,
                           0x00,                // Message bytes
                           };
struct I2CMsg GY521_i2cMsgIn  = {MSG_STATUS_SEND_NOSTOP,
                           SLAVE_ADDRESS,
                           14,
                           MPU6050_RA_ACCEL_XOUT_H,
                           };

struct I2CMsg *currentMsgPtr;                   // Used in interrupt

uint16_t passCount = 0;
uint16_t failCount = 0;

//
// Function Prototypes
//
void initI2C(void);
uint16_t readData(struct I2CMsg *msg);
uint16_t writeData(struct I2CMsg *msg);
uint16_t error;


void GY521_init();
uint16_t GY521_requestEnable();
uint16_t GY521_requestEnableCompleted();
uint16_t GY521_requestData();
uint16_t GY521_requestDataCompleted();
float32_t GY521_getTempCentigrad();

void fail(void);
void pass(void);

__interrupt void i2cAISR(void);

//
// Main
//
void main(void)
{
    //
    // Initialize device clock and peripherals
    //
    Device_init();

    //
    // Disable pin locks and enable internal pullups.
    //
    Device_initGPIO();

    //
    // Initialize GPIOs 32 and 33 for use as SDA A and SCL A respectively
    //
    GPIO_setPinConfig(GPIO_105_SCLA);
    GPIO_setPadConfig(105, GPIO_PIN_TYPE_PULLUP);
    GPIO_setQualificationMode(105, GPIO_QUAL_ASYNC);

    GPIO_setPinConfig(GPIO_104_SDAA);
    GPIO_setPadConfig(104, GPIO_PIN_TYPE_PULLUP);
    GPIO_setQualificationMode(104, GPIO_QUAL_ASYNC);

    //
    // Initialize PIE and clear PIE registers. Disable CPU interrupts.
    //
    Interrupt_initModule();

    //
    // Initialize the PIE vector table with pointers to the shell Interrupt
    // Service Routines (ISR).
    //
    Interrupt_initVectorTable();

    //
    // Interrupts that are used in this example are re-mapped to ISR functions
    // found within this file.
    //
    Interrupt_register(INT_I2CA, &i2cAISR);

    //
    // Set I2C use, initializing it for FIFO mode
    //
    initI2C();

    //
    // Enable interrupts required for this example
    //
    Interrupt_enable(INT_I2CA);

    //
    // Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
    //
    EINT;
    ERTM;

    GY521_init();

    GY521_requestEnable();
    while(GY521_requestEnableCompleted() != SUCCESS);
    ESTOP0;

    DEVICE_DELAY_US(100000);

    while(1)
    {
        GY521_requestData();
        while(GY521_requestDataCompleted() != SUCCESS);

        GY521_getTempCentigrad();
        DEVICE_DELAY_US(100000);

    }

}

//
// initI2C - Function to configure I2C A in FIFO mode.
//
void
initI2C()
{
    //
    // Must put I2C into reset before configuring it
    //
    I2C_disableModule(I2CA_BASE);

    //
    // I2C configuration. Use a 400kHz I2CCLK with a 50% duty cycle.
    //
    I2C_initMaster(I2CA_BASE, DEVICE_SYSCLK_FREQ, 100000, I2C_DUTYCYCLE_50);
    I2C_setBitCount(I2CA_BASE, I2C_BITCOUNT_8);
    I2C_setSlaveAddress(I2CA_BASE, SLAVE_ADDRESS);
    I2C_setEmulationMode(I2CA_BASE, I2C_EMULATION_FREE_RUN);

    //
    // Enable stop condition and register-access-ready interrupts
    //
    I2C_enableInterrupt(I2CA_BASE, I2C_INT_STOP_CONDITION |
                        I2C_INT_REG_ACCESS_RDY);

    //
    // FIFO configuration
    //
    I2C_enableFIFO(I2CA_BASE);
    I2C_clearInterruptStatus(I2CA_BASE, I2C_INT_RXFF | I2C_INT_TXFF);

    //
    // Configuration complete. Enable the module.
    //
    I2C_enableModule(I2CA_BASE);
}


void GY521_init()
{
    uint16_t i = 0;

    //
    // Clear incoming message buffer
    //
    for (i = 0; i < MAX_BUFFER_SIZE; i++)
    {
        GY521_i2cMsgIn.msgBuffer[i] = 0x0000;
    }

    GY521_i2cMsgOut.msgStatus = MSG_STATUS_INACTIVE;
    GY521_i2cMsgIn.msgStatus = MSG_STATUS_INACTIVE;
}

uint16_t GY521_requestEnable()
{

    if (GY521_i2cMsgOut.msgStatus != MSG_STATUS_INACTIVE)
    {
        return ERROR_ENABLE_FAIL;
    }

    GY521_i2cMsgOut.msgStatus = MSG_STATUS_SEND_WITHSTOP;
    //
    // Send the data to the EEPROM
    //
    error = writeData(&GY521_i2cMsgOut);

    //
    // If communication is correctly initiated, set msg status to busy
    // and update currentMsgPtr for the interrupt service routine.
    // Otherwise, do nothing and try again next loop. Once message is
    // initiated, the I2C interrupts will handle the rest. See the
    // function i2cAISR().
    //
    if(error == SUCCESS)
    {

    }
    else
    {
        ESTOP0;
        return ERROR_ENABLE_FAIL;
    }


    return SUCCESS;
}

uint16_t GY521_requestEnableCompleted()
{
    if (GY521_i2cMsgOut.msgStatus != MSG_STATUS_INACTIVE)
    {
        return ERROR_ENABLE_FAIL;
    }

    return SUCCESS;
}

uint16_t GY521_requestData()
{
    if(GY521_i2cMsgIn.msgStatus != MSG_STATUS_INACTIVE)
    {
        ESTOP0;
        return ERROR_DATAREAD_FAIL;
    }

    GY521_i2cMsgIn.msgStatus = MSG_STATUS_SEND_NOSTOP;

    //
    // Send EEPROM address setup
    //
    error = readData(&GY521_i2cMsgIn);
    if (error != SUCCESS)
    {
        //
        // Maybe setup an attempt counter to break an infinite
        // while loop. The EEPROM will send back a NACK while it is
        // performing a write operation. Even though the write
        // is complete at this point, the EEPROM could still be
        // busy programming the data. Therefore, multiple
        // attempts are necessary.
        //
        failCount++;
        ESTOP0;
        return ERROR_DATAREAD_FAIL;
    }
    return SUCCESS;
}

uint16_t GY521_requestDataCompleted()
{
    if (GY521_i2cMsgIn.msgStatus != MSG_STATUS_INACTIVE)
    {
        return ERROR_DATAREAD_FAIL;
    }

    return SUCCESS;
}

float32_t GY521_getTempCentigrad()
{
    temp_centigrad = ((float32_t)temp)/340.00 + 36.53;
    return temp_centigrad;
}

//
// writeData - Function to send the data that is to be written to the EEPROM
//
uint16_t
writeData(struct I2CMsg *msg)
{
    uint16_t i;

    //
    // Wait until the STP bit is cleared from any previous master
    // communication. Clearing of this bit by the module is delayed until after
    // the SCD bit is set. If this bit is not checked prior to initiating a new
    // message, the I2C could get confused.
    //
    if(I2C_getStopConditionStatus(I2CA_BASE))
    {
        return(ERROR_STOP_NOT_READY);
    }

    //
    // Setup slave address
    //
    I2C_setSlaveAddress(I2CA_BASE, SLAVE_ADDRESS);

    //
    // Check if bus busy
    //
    if(I2C_isBusBusy(I2CA_BASE))
    {
        return(ERROR_BUS_BUSY);
    }

    //
    // Set message pointer used in interrupt to point to outgoing message
    //
    currentMsgPtr = msg;

    //
    // Setup number of bytes to send msgBuffer and address
    //
    I2C_setDataCount(I2CA_BASE, (msg->numBytes + 1));

    //
    // Setup data to send
    //
    I2C_putData(I2CA_BASE, msg->memoryAddr);

    for (i = 0; i < msg->numBytes; i++)
    {
        I2C_putData(I2CA_BASE, msg->msgBuffer[i]);
    }

    //
    // Send start as master transmitter
    //
    I2C_setConfig(I2CA_BASE, I2C_MASTER_SEND_MODE);
    I2C_sendStartCondition(I2CA_BASE);
    I2C_sendStopCondition(I2CA_BASE);

    msg->msgStatus = MSG_STATUS_WRITE_BUSY;

    return(SUCCESS);
}

//
// readData - Function to prepare for the data that is to be read from the EEPROM
//
uint16_t
readData(struct I2CMsg *msg)
{
    //
    // Wait until the STP bit is cleared from any previous master
    // communication. Clearing of this bit by the module is delayed until after
    // the SCD bit is set. If this bit is not checked prior to initiating a new
    // message, the I2C could get confused.
    //
    if(I2C_getStopConditionStatus(I2CA_BASE))
    {
        return(ERROR_STOP_NOT_READY);
    }

    currentMsgPtr = msg;

    //
    // Setup slave address
    //
    I2C_setSlaveAddress(I2CA_BASE, SLAVE_ADDRESS);

    //
    // If we are in the the address setup phase, send the address without a
    // stop condition.
    //
    if(msg->msgStatus == MSG_STATUS_SEND_NOSTOP)
    {
        //
        // Check if bus busy
        //
        if(I2C_isBusBusy(I2CA_BASE))
        {
            return(ERROR_BUS_BUSY);
        }

        //
        // Send data to setup EEPROM address
        //
        I2C_setDataCount(I2CA_BASE, 1);
        I2C_putData(I2CA_BASE, msg->memoryAddr);
        I2C_setConfig(I2CA_BASE, I2C_MASTER_SEND_MODE);
        I2C_sendStartCondition(I2CA_BASE);

        //
        // Update current message pointer and message status
        //
        msg->msgStatus = MSG_STATUS_SEND_NOSTOP_BUSY;
    }
    else if(msg->msgStatus == MSG_STATUS_RESTART)
    {
        //
        // Address setup phase has completed. Now setup how many bytes expected
        // and send restart as master-receiver.
        //
        I2C_setDataCount(I2CA_BASE, (msg->numBytes));
        I2C_setConfig(I2CA_BASE, I2C_MASTER_RECEIVE_MODE);
        I2C_sendStartCondition(I2CA_BASE);
        I2C_sendStopCondition(I2CA_BASE);
    }

    return(SUCCESS);
}

//
// i2cAISR - I2C A ISR (non-FIFO)
//
__interrupt void
i2cAISR(void)
{
    I2C_InterruptSource intSource;
    uint16_t i;

    //
    // Read interrupt source
    //
    intSource = I2C_getInterruptSource(I2CA_BASE);

    //
    // Interrupt source = stop condition detected
    //
    if(intSource == I2C_INTSRC_STOP_CONDITION)
    {
        //
        // If completed message was writing data, reset msg to inactive state
        //
        if(currentMsgPtr->msgStatus == MSG_STATUS_WRITE_BUSY)
        {
            currentMsgPtr->msgStatus = MSG_STATUS_INACTIVE;
        }
        else
        {
            //
            // If a message receives a NACK during the address setup portion of
            // the EEPROM read, the code further below included in the register
            // access ready interrupt source code will generate a stop
            // condition. After the stop condition is received (here), set the
            // message status to try again. User may want to limit the number
            // of retries before generating an error.
            //
            if(currentMsgPtr->msgStatus == MSG_STATUS_SEND_NOSTOP_BUSY)
            {
                currentMsgPtr->msgStatus = MSG_STATUS_SEND_NOSTOP;
            }
            //
            // If completed message was reading EEPROM data, reset message to
            // inactive state and read data from FIFO.
            //
            else if(currentMsgPtr->msgStatus == MSG_STATUS_READ_BUSY)
            {
                currentMsgPtr->msgStatus = MSG_STATUS_INACTIVE;
                for(i=0; i < currentMsgPtr->numBytes; i++)
                {
                    currentMsgPtr->msgBuffer[i] = I2C_getData(I2CA_BASE);
                }

                accX = (currentMsgPtr->msgBuffer[0]<<8)|(currentMsgPtr->msgBuffer[1]);
                accY = (currentMsgPtr->msgBuffer[2]<<8)|(currentMsgPtr->msgBuffer[3]);
                accZ = (currentMsgPtr->msgBuffer[4]<<8)|(currentMsgPtr->msgBuffer[5]);
                temp = (currentMsgPtr->msgBuffer[6]<<8)|(currentMsgPtr->msgBuffer[7]);
                gyrX = (currentMsgPtr->msgBuffer[8]<<8)|(currentMsgPtr->msgBuffer[9]);
                gyrY = (currentMsgPtr->msgBuffer[10]<<8)|(currentMsgPtr->msgBuffer[11]);
                gyrZ = (currentMsgPtr->msgBuffer[12]<<8)|(currentMsgPtr->msgBuffer[13]);
            }
        }
    }
    //
    // Interrupt source = Register Access Ready
    //
    // This interrupt is used to determine when the EEPROM address setup
    // portion of the read data communication is complete. Since no stop bit
    // is commanded, this flag tells us when the message has been sent
    // instead of the SCD flag.
    //
    else if(intSource == I2C_INTSRC_REG_ACCESS_RDY)
    {
        //
        // If a NACK is received, clear the NACK bit and command a stop.
        // Otherwise, move on to the read data portion of the communication.
        //
        if((I2C_getStatus(I2CA_BASE) & I2C_STS_NO_ACK) != 0)
        {
            I2C_sendStopCondition(I2CA_BASE);
            I2C_clearStatus(I2CA_BASE, I2C_STS_NO_ACK);
            failCount++;
        }
        else if(currentMsgPtr->msgStatus == MSG_STATUS_SEND_NOSTOP_BUSY)
        {
            currentMsgPtr->msgStatus = MSG_STATUS_RESTART;
            error = readData(currentMsgPtr);
            //
            // Read data portion
            //
            while(error != SUCCESS)
            {
                //
                // Maybe setup an attempt counter to break an infinite
                // while loop.
                //
            }

            //
            // Update current message pointer and message status
            //
            currentMsgPtr->msgStatus = MSG_STATUS_READ_BUSY;
        }
    }
    else
    {
        //
        // Generate some error from invalid interrupt source
        //
        asm("   ESTOP0");
    }

    //
    // Issue ACK to enable future group 8 interrupts
    //
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP8);
}

//
// End of File
//

