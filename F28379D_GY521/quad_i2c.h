
#ifndef QUAD_I2C_H_
#define QUAD_I2C_H_

#include "driverlib.h"
#include "device.h"

#define MAX_BUFFER_SIZE             15      // Max is currently 15 because of
                                            // 1 address byte and the 16-byte
                                            // FIFO

//
// Typedefs
//
struct I2CMsg
{
    uint16_t msgStatus;                  // Word stating what state msg is in.
                                         // See MSG_STATUS_* defines above.
    uint16_t slaveAddr;                  // Slave address tied to the message.
    uint16_t numBytes;                   // Number of valid bytes in message.
    uint16_t memoryAddr;                 // EEPROM address of data associated
                                         // with message (high byte).

    uint16_t msgBuffer[MAX_BUFFER_SIZE]; // Array holding message data.

};



#endif /* QUAD_I2C_H_ */
