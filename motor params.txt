motorVars.offsets_I_A.value[0]	float	-15.9867058	0x000004EA@Data	
motorVars.offsets_I_A.value[1]	float	-15.9609156	0x000004EC@Data	
motorVars.offsets_I_A.value[2]	float	-16.117197	0x000004EE@Data	
motorVars.offsets_V_V.value[0]	float	0.939531147	0x000004F0@Data	
motorVars.offsets_V_V.value[1]	float	0.947219968	0x000004F2@Data	
motorVars.offsets_V_V.value[2]	float	0.938152313	0x000004F4@Data	

//! \brief ADC current offsets for A, B, and C phases
#define USER_M1_IA_OFFSET_A    (-15.9867058)    // ~=0.5*USER_M1_ADC_FULL_SCALE_CURRENT_A
#define USER_M1_IB_OFFSET_A    (-15.9609156)    // ~=0.5*USER_M1_ADC_FULL_SCALE_CURRENT_A
#define USER_M1_IC_OFFSET_A    (-16.117197)     // ~=0.5*USER_M1_ADC_FULL_SCALE_CURRENT_A


//! \brief ADC voltage offsets for A, B, and C phases
#define USER_M1_VA_OFFSET_V    (0.939531147)          // ~=1.0
#define USER_M1_VB_OFFSET_V    (0.947219968)          // ~=1.0
#define USER_M1_VC_OFFSET_V    (0.938152313)          // ~=1.0

//! \brief ADC current offsets for A, B, and C phases
#define USER_M2_IA_OFFSET_A    (-15.9867058)    // ~=0.5*USER_M2_ADC_FULL_SCALE_CURRENT_A
#define USER_M2_IB_OFFSET_A    (-15.9609156)    // ~=0.5*USER_M2_ADC_FULL_SCALE_CURRENT_A
#define USER_M2_IC_OFFSET_A    (-16.117197)     // ~=0.5*USER_M2_ADC_FULL_SCALE_CURRENT_A


//! \brief ADC voltage offsets for A, B, and C phases
#define USER_M2_VA_OFFSET_V    (0.939531147)          // ~=1.0
#define USER_M2_VB_OFFSET_V    (0.947219968)          // ~=1.0
#define USER_M2_VC_OFFSET_V    (0.938152313)          // ~=1.0


motorVars.Rr_Ohm	float	0.0	0x00000474@Data	
motorVars.Rs_Ohm	float	0.0867254958	0x00000476@Data	
motorVars.Ls_d_H	float	1.64798112e-05	0x0000047A@Data	
motorVars.Ls_q_H	float	1.64798112e-05	0x0000047C@Data	
motorVars.RsOnLine_Ohm	float	0.0867254958	0x00000478@Data	
motorVars.RoverL_rps	float	5003.02344	0x0000047E@Data	
motorVars.flux_VpHz	float	0.00558881834	0x00000480@Data	
motorVars.magneticCurrent_A	float	0.0	0x00000472@Data	

motorVars.Rr_Ohm	float	0.0	0x00000474@Data	
motorVars.Rs_Ohm	float	0.0870432928	0x00000476@Data	
motorVars.Ls_d_H	float	1.69242448e-05	0x0000047A@Data	
motorVars.Ls_q_H	float	1.69242448e-05	0x0000047C@Data	
motorVars.RsOnLine_Ohm	float	0.0870432928	0x00000478@Data	
motorVars.RoverL_rps	float	4955.47998	0x0000047E@Data	
motorVars.flux_VpHz	float	0.0056619416	0x00000480@Data	
motorVars.magneticCurrent_A	float	0.0	0x00000472@Data	

#define USER_MOTOR_TYPE                   MOTOR_TYPE_PM
#define USER_MOTOR_NUM_POLE_PAIRS         (6)
#define USER_MOTOR_Rr_Ohm                 (0.0)
#define USER_MOTOR_Rs_Ohm                 (0.0870432928)
#define USER_MOTOR_Ls_d_H                 (1.69242448e-05)
#define USER_MOTOR_Ls_q_H                 (1.69242448e-05)
#define USER_MOTOR_RATED_FLUX_VpHz        (0.0056619416)
#define USER_MOTOR_MAGNETIZING_CURRENT_A  (0.0)
#define USER_MOTOR_RES_EST_CURRENT_A      (3.0)
#define USER_MOTOR_IND_EST_CURRENT_A      (-3.0)
#define USER_MOTOR_MAX_CURRENT_A          (15.0)
#define USER_MOTOR_FLUX_EXC_FREQ_Hz       (40.0)

// Number of lines on the motor's quadrature encoder
#define USER_MOTOR_NUM_ENC_SLOTS          (NULL)

#define USER_MOTOR_FREQ_MIN_HZ             (5.0)           // Hz
#define USER_MOTOR_FREQ_MAX_HZ            (300.0)         // Hz

#define USER_MOTOR_FREQ_LOW_HZ            (10.0)          // Hz
#define USER_MOTOR_FREQ_HIGH_HZ           (400.0)         // Hz
#define USER_MOTOR_VOLT_MIN_V             (7.0)           // Volt
#define USER_MOTOR_VOLT_MAX_V             (12.0)          // Volt


#define USER_M1_MOTOR_TYPE                   MOTOR_TYPE_PM
#define USER_M1_MOTOR_NUM_POLE_PAIRS         (6)
#define USER_M1_MOTOR_Rr_Ohm                 (0.0)
#define USER_M1_MOTOR_Rs_Ohm                 (0.0870432928)
#define USER_M1_MOTOR_Ls_d_H                 (1.69242448e-05)
#define USER_M1_MOTOR_Ls_q_H                 (1.69242448e-05)
#define USER_M1_MOTOR_RATED_FLUX_VpHz        (0.0056619416)
#define USER_M1_MOTOR_MAGNETIZING_CURRENT_A  (0.0)
#define USER_M1_MOTOR_RES_EST_CURRENT_A      (3.0)
#define USER_M1_MOTOR_IND_EST_CURRENT_A      (-3.0)
#define USER_M1_MOTOR_MAX_CURRENT_A          (15.0)
#define USER_M1_MOTOR_FLUX_EXC_FREQ_Hz       (40.0)

// Number of lines on the motor's quadrature encoder
#define USER_M1_MOTOR_NUM_ENC_SLOTS          (NULL)

#define USER_M1_MOTOR_MIN_MAX_HZ             (5.0)           // Hz
#define USER_M1_MOTOR_FREQ_MAX_HZ            (300.0)         // Hz

#define USER_M1_MOTOR_FREQ_LOW_HZ            (10.0)          // Hz
#define USER_M1_MOTOR_FREQ_HIGH_HZ           (400.0)         // Hz
#define USER_M1_MOTOR_VOLT_MIN_V             (7.0)           // Volt
#define USER_M1_MOTOR_VOLT_MAX_V             (12.0)          // Volt


#define USER_M2_MOTOR_TYPE                   MOTOR_TYPE_PM
#define USER_M2_MOTOR_NUM_POLE_PAIRS         (6)
#define USER_M2_MOTOR_Rr_Ohm                 (0.0)
#define USER_M2_MOTOR_Rs_Ohm                 (0.0870432928)
#define USER_M2_MOTOR_Ls_d_H                 (1.69242448e-05)
#define USER_M2_MOTOR_Ls_q_H                 (1.69242448e-05)
#define USER_M2_MOTOR_RATED_FLUX_VpHz        (0.0056619416)
#define USER_M2_MOTOR_MAGNETIZING_CURRENT_A  (0.0)
#define USER_M2_MOTOR_RES_EST_CURRENT_A      (3.0)
#define USER_M2_MOTOR_IND_EST_CURRENT_A      (-3.0)
#define USER_M2_MOTOR_MAX_CURRENT_A          (15.0)
#define USER_M2_MOTOR_FLUX_EXC_FREQ_Hz       (40.0)

// Number of lines on the motor's quadrature encoder
#define USER_M2_MOTOR_NUM_ENC_SLOTS          (NULL)

#define USER_M2_MOTOR_MIN_MAX_HZ             (5.0)           // Hz
#define USER_M2_MOTOR_FREQ_MAX_HZ            (300.0)         // Hz

#define USER_M2_MOTOR_FREQ_LOW_HZ            (10.0)          // Hz
#define USER_M2_MOTOR_FREQ_HIGH_HZ           (400.0)         // Hz
#define USER_M2_MOTOR_VOLT_MIN_V             (7.0)           // Volt
#define USER_M2_MOTOR_VOLT_MAX_V             (12.0)          // Volt