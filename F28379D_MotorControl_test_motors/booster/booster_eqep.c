
//
// Included Files
//
#include "F28x_Project.h"
#include "driverlib.h"
#include "device.h"
#include "booster_motor.h"
#include "booster_eqep.h"

MotorInfo LeftMotor;
MotorInfo RightMotor;

__interrupt void EQEP1_interrupt();
__interrupt void EQEP2_interrupt();

void booster_eqep_initEQEP(uint32_t base);

//
// Function to configure eQEP1.
//
void booster_eqep_initAllEQEP(void)
{

    booster_eqep_initEQEP(EQEP1_BASE);
    //Register the interrupt
    Interrupt_register(INT_EQEP1, &EQEP1_interrupt);
    //Enable Interrupt
    Interrupt_enable(INT_EQEP1);

    booster_eqep_initEQEP(EQEP2_BASE);
    //Register the interrupt
    Interrupt_register(INT_EQEP2, &EQEP2_interrupt);
    //Enable Interrupt
    Interrupt_enable(INT_EQEP2);


}

void booster_eqep_initEQEP(uint32_t base)
{
    //
    // Configure the decoder for quadrature count mode
    //
    EQEP_setDecoderConfig(base, (EQEP_CONFIG_1X_RESOLUTION |
                                       EQEP_CONFIG_QUADRATURE |
                                       EQEP_CONFIG_NO_SWAP));

    EQEP_setEmulationMode(base, EQEP_EMULATIONMODE_RUNFREE);

    //
    // Configure the position counter to reset on an index event
    //
    EQEP_setPositionCounterConfig(base, EQEP_POSITION_RESET_UNIT_TIME_OUT,
                                  MAX_POSITION);

    //
    // Enable the unit timer, setting the frequency to 1000 Hz
    //
    EQEP_enableUnitTimer(base, (DEVICE_SYSCLK_FREQ/10));

    EQEP_enableInterrupt(base, EQEP_INT_UNIT_TIME_OUT);

    //
    // Configure the position counter to be latched on a unit time out
    //
    EQEP_setLatchMode(base, EQEP_LATCH_UNIT_TIME_OUT);


    //
    // Enable the eQEP1 module
    //
    EQEP_enableModule(base);

}

__interrupt void EQEP1_interrupt()
{
    uint16_t flags = EQEP_getInterruptStatus(EQEP1_BASE);
    if (flags & EQEP_INT_UNIT_TIME_OUT)
    {
        LeftMotor.currentPosition = EQEP_getPositionLatch(EQEP1_BASE);
        if (LeftMotor.currentPosition < MAX_POSITION/2)
        {
            //We moved forward
            LeftMotor.traveledDirection = MOTOR_FORWARD;
            LeftMotor.displacedPosition = LeftMotor.currentPosition;

        }
        else
        {
            //We moved Backward
            LeftMotor.traveledDirection = MOTOR_REVERSE;
            LeftMotor.displacedPosition = MAX_POSITION - LeftMotor.currentPosition;

        }
        LeftMotor.currentDirection = (EQep1Regs.QEPSTS.bit.QDF == 1)? MOTOR_FORWARD: MOTOR_REVERSE;

    }

    EQEP_clearInterruptStatus(EQEP1_BASE, flags);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP5);
}


__interrupt void EQEP2_interrupt()
{
    uint16_t flags = EQEP_getInterruptStatus(EQEP2_BASE);
    if (flags & EQEP_INT_UNIT_TIME_OUT)
    {
        RightMotor.currentPosition = EQEP_getPositionLatch(EQEP2_BASE);
        if (RightMotor.currentPosition < MAX_POSITION/2)
        {
            //We moved forward
            RightMotor.traveledDirection = MOTOR_FORWARD;
            RightMotor.displacedPosition = RightMotor.currentPosition;

        }
        else
        {
            //We moved Backward
            RightMotor.traveledDirection = MOTOR_REVERSE;
            RightMotor.displacedPosition = MAX_POSITION - RightMotor.currentPosition;

        }
        RightMotor.currentDirection = (EQep2Regs.QEPSTS.bit.QDF == 1)? MOTOR_FORWARD: MOTOR_REVERSE;

    }

    EQEP_clearInterruptStatus(EQEP2_BASE, flags);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP5);
}
