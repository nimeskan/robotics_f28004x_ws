/*
 * motor.h
 *
 *  Created on: Sep 30, 2018
 *      Author: a0225962
 */

#ifndef BOOSTER_BOOSTER_MOTOR_H_
#define BOOSTER_BOOSTER_MOTOR_H_


#define LAST_MOTOR_INDEX        6
#define FIRST_MOTOR_INDEX       1


typedef enum{
    MOTOR_1 = 1,
    MOTOR_2 = 2,
    MOTOR_3 = 3,
    MOTOR_4 = 4,
    MOTOR_5 = 5,
    MOTOR_6 = 6
} MotorNumber;

typedef enum{
    MOTOR_FORWARD = 0,
    MOTOR_REVERSE = 1
} MotorDirection;



#endif /* BOOSTER_BOOSTER_MOTOR_H_ */
