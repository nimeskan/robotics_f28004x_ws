
#ifndef BOOSTER_BOOSTER_EQEP_H_
#define BOOSTER_BOOSTER_EQEP_H_

#define MAX_POSITION 0xFFFFFFFF

typedef struct{
    MotorDirection currentDirection;
    uint32_t currentPosition;
    uint32_t displacedPosition;

    uint32_t speed;
    uint32_t distanceTraveled;
    MotorDirection traveledDirection;

} MotorInfo;

extern MotorInfo LeftMotor;
extern MotorInfo RightMotor;

void booster_eqep_initAllEQEP(void);

#endif /* BOOSTER_BOOSTER_EQEP_H_ */
