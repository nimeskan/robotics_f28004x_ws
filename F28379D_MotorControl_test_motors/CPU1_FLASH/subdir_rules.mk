################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
%.obj: ../%.asm $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/c2000_compilers/ti-cgt-c2000_17.6.0.STS/bin/cl2000" -v28 -ml -mt --cla_support=cla1 --float_support=fpu32 --tmu_support=tmu0 --vcu_support=vcu2 -Ooff --include_path="C:/Users/a0225962/workspace_robotics/F28379D_MotorControl_test_motors" --include_path="C:/ti/c2000/latestc2k/C2000Ware_2_00_00_03/device_support/f2837xd/common/include" --include_path="C:/ti/c2000/latestc2k/C2000Ware_2_00_00_03/device_support/f2837xd/headers/include" --include_path="C:/Users/a0225962/workspace_robotics/F28379D_MotorControl_test_motors/device" --include_path="C:/ti/c2000/latestc2k/C2000Ware_2_00_00_03/driverlib/f2837xd/driverlib" --include_path="C:/ti/c2000_compilers/ti-cgt-c2000_17.6.0.STS/include" --define=_FLASH --define=_LAUNCHXL_F28379D --define=DEBUG --define=_DUAL_HEADERS --define=CPU1 --diag_suppress=10063 --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

%.obj: ../%.c $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C2000 Compiler'
	"C:/ti/c2000_compilers/ti-cgt-c2000_17.6.0.STS/bin/cl2000" -v28 -ml -mt --cla_support=cla1 --float_support=fpu32 --tmu_support=tmu0 --vcu_support=vcu2 -Ooff --include_path="C:/Users/a0225962/workspace_robotics/F28379D_MotorControl_test_motors" --include_path="C:/ti/c2000/latestc2k/C2000Ware_2_00_00_03/device_support/f2837xd/common/include" --include_path="C:/ti/c2000/latestc2k/C2000Ware_2_00_00_03/device_support/f2837xd/headers/include" --include_path="C:/Users/a0225962/workspace_robotics/F28379D_MotorControl_test_motors/device" --include_path="C:/ti/c2000/latestc2k/C2000Ware_2_00_00_03/driverlib/f2837xd/driverlib" --include_path="C:/ti/c2000_compilers/ti-cgt-c2000_17.6.0.STS/include" --define=_FLASH --define=_LAUNCHXL_F28379D --define=DEBUG --define=_DUAL_HEADERS --define=CPU1 --diag_suppress=10063 --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '


