
//
// Included Files
//
#include "F28x_Project.h"
#include "driverlib.h"
#include "device.h"
#include "booster/booster_motor.h"
#include "booster/booster_pinout.h"
#include "booster/booster_epwm.h"
#include "booster/booster_eqep.h"

uint16_t motor_speed = 60;
MotorNumber motor = MOTOR_1;
uint16_t doCMD = 0;
uint16_t duration = 800;
MotorDirection dir = MOTOR_REVERSE;

#define LEFT_MOTOR      MOTOR_1 //Bot
#define RIGHT_MOTOR     MOTOR_2 //Top

//
// Main
//
void main(void)
{
   // Initialize device clock and peripherals
   Device_init();
   // Disable pin locks and enable internal pull ups.
   Device_initGPIO();
   // Initialize PIE and clear PIE registers. Disables CPU interrupts.
   Interrupt_initModule();
   // Initialize the PIE vector table with pointers to the shell Interrupt
   Interrupt_initVectorTable();

   booster_Pinout_init();

   booster_epwm_initAllEPWMs();
   // Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
   EINT;
   ERTM;

   booster_eqep_initAllEQEP();

   booster_epwm_startMotor(LEFT_MOTOR, MOTOR_FORWARD, 0);
   booster_epwm_startMotor(RIGHT_MOTOR, MOTOR_FORWARD, 0);
   while(1)
   {



   }
}

//
// End of File
//
