
//
// Included Files
//
#include "F28x_Project.h"
#include "driverlib.h"
#include "device.h"
#include "booster/booster_motor.h"
#include "booster/booster_pinout.h"
#include "booster/booster_epwm.h"
#include "booster/booster_eqep.h"

uint16_t motor_speed = 75;
MotorNumber motor = MOTOR_1;
uint16_t doCMD = 0;
uint16_t duration = 800;
int32_t leftTicks = 0;
int32_t RightTicks = 0;
MotorDirection dir = MOTOR_REVERSE;



//
// Main
//
void main(void)
{
   // Initialize device clock and peripherals
   Device_init();
   // Disable pin locks and enable internal pull ups.
   Device_initGPIO();
   // Initialize PIE and clear PIE registers. Disables CPU interrupts.
   Interrupt_initModule();
   // Initialize the PIE vector table with pointers to the shell Interrupt
   Interrupt_initVectorTable();

   booster_Pinout_init();

   booster_epwm_initAllEPWMs();
   // Enable Global Interrupt (INTM) and realtime interrupt (DBGM)
   EINT;
   ERTM;

   booster_eqep_initAllEQEP();


   while(1)
   {
       if (doCMD)
       {
           booster_motor_moveTwoMotorsForTicks(&RightMotor, &LeftMotor, motor_speed, RightTicks, leftTicks);
           doCMD = 0;
       }
   }
}

//
// End of File
//
