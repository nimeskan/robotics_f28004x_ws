
#ifndef QUAD_DUAL_MOTOR_H_
#define QUAD_DUAL_MOTOR_H_

#include <src_quad/quad_sci.h>

typedef void (*COMMAND_FXN)(uint16_t *data, uint16_t sizeData);

void CommandReceived(uint32_t base, uint16_t *data, uint16_t sizeData);

inline void motor_command_start(uint16_t *data, uint16_t sizeData){

}

inline void motor_command_change_speed(uint16_t *data, uint16_t sizeData){

}

inline void motor_command_stop(uint16_t *data, uint16_t sizeData){

}

#endif /* QUAD_DUAL_MOTOR_H_ */
