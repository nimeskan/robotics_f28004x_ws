/*
 * booster_sci.h
 *
 *  Created on: Nov 2, 2018
 *      Author: a0225962
 */

#ifndef BOOSTER_BOOSTER_SCI_H_
#define BOOSTER_BOOSTER_SCI_H_

extern __interrupt void scibRxISR(void);

typedef void (*SCI_CallbackFxn)(uint32_t base, uint16_t *data, uint16_t sizeData);

#define MAX_SCI_CHARS   16

extern uint16_t NewCommandAvailable;
extern uint16_t CommandReceivedData[MAX_SCI_CHARS];

void quad_sci_init(SCI_CallbackFxn callback);




#endif /* BOOSTER_BOOSTER_SCI_H_ */
