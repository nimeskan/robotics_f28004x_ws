
#ifndef QUAD_DUAL_MOTOR_H_
#define QUAD_DUAL_MOTOR_H_

#include <src_quad/quad_sci.h>
#include <src_quad/quad_i2c.h>

typedef void (*COMMAND_FXN)(uint16_t *data, uint16_t sizeData);

void motor_command_systemVars(uint16_t *data, uint16_t sizeData);
void motor_command_change_speed(uint16_t *data, uint16_t sizeData);
void motor_command_stop(uint16_t *data, uint16_t sizeData);
void gy521_command_send_data(uint16_t *data, uint16_t sizeData);
#endif /* QUAD_DUAL_MOTOR_H_ */
