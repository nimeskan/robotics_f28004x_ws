
#ifndef QUAD_I2C_H_
#define QUAD_I2C_H_

#include "driverlib.h"
#include "device.h"

#define MAX_BUFFER_SIZE             15      // Max is currently 15 because of
                                            // 1 address byte and the 16-byte
                                            // FIFO

//
// Typedefs
//
struct I2CMsg
{
    uint16_t msgStatus;                  // Word stating what state msg is in.
                                         // See MSG_STATUS_* defines above.
    uint16_t slaveAddr;                  // Slave address tied to the message.
    uint16_t numBytes;                   // Number of valid bytes in message.
    uint16_t memoryAddr;                 // EEPROM address of data associated
                                         // with message (high byte).

    uint16_t msgBuffer[MAX_BUFFER_SIZE]; // Array holding message data.

};

//
// Error messages for read and write functions
//
#define ERROR_BUS_BUSY              0x1000
#define ERROR_STOP_NOT_READY        0x5555
#define ERROR_ENABLE_FAIL           0x1001
#define ERROR_DATAREAD_FAIL         0x1002
#define SUCCESS                     0x0000


#define GY521_X_AXIS    0
#define GY521_Y_AXIS    1
#define GY521_Z_AXIS    2

void quad_i2c_init();
void GY521_init();
uint16_t GY521_requestEnable();
uint16_t GY521_requestEnableCompleted();
uint16_t GY521_requestData();
uint16_t GY521_requestDataCompleted();
float32_t GY521_getTempCentigrad();
void GY521_getAccelerometerXYZ(int16_t *accelrometerXYZ);
void GY521_getGyroscopeXYZ(int16_t *gyroscopeXYZ);

void GY521_getAccelerometerXYZRaw(uint16_t *accelrometerXYZ);
void GY521_getGyroscopeXYZRaw(uint16_t *gyroscopeXYZ);

uint16_t GY521_getAccXRaw();
uint16_t GY521_getAccYRaw();
uint16_t GY521_getAccZRaw();

uint16_t GY521_getGyrXRaw();
uint16_t GY521_getGyrYRaw();
uint16_t GY521_getGyrZRaw();

uint16_t GY521_getTempRaw();

#endif /* QUAD_I2C_H_ */
