/*
 * booster_sci.h
 *
 *  Created on: Nov 2, 2018
 *      Author: a0225962
 */

#ifndef BOOSTER_BOOSTER_SCI_H_
#define BOOSTER_BOOSTER_SCI_H_

extern __interrupt void scibRxISR(void);

#define MAX_SCI_CHARS   16
#define NUMBER_OF_BYTES_IN_MSG  10
#define LAST_BYTE_INDEX_IN_MSG  NUMBER_OF_BYTES_IN_MSG-1

extern uint16_t NewCommandAvailable;
extern uint16_t CommandReceivedData[MAX_SCI_CHARS];

void quad_sci_init();
void quad_sci_write(uint16_t * data, uint16_t size);


#endif /* BOOSTER_BOOSTER_SCI_H_ */
