/*
 * booster_sci.c
 *
 *  Created on: Nov 2, 2018
 *      Author: a0225962
 */

#include "driverlib.h"
#include "device.h"
#include <src_quad/quad_sci.h>

uint16_t NewCommandAvailable = 0;
uint16_t CommandReceivedData[MAX_SCI_CHARS] = {0};


#pragma CODE_SECTION(scibRxISR, ".TI.ramfunc");
__interrupt void scibRxISR(void);


void quad_sci_init()
{

    //
    // GPIO14 is the SCI Tx pin.
    //
    GPIO_setMasterCore(14, GPIO_CORE_CPU1);
    GPIO_setPinConfig(GPIO_14_SCITXDB);
    GPIO_setDirectionMode(14, GPIO_DIR_MODE_IN);
    GPIO_setPadConfig(14, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(14, GPIO_QUAL_ASYNC);

    //
    // GPIO15 is the SCI Rx pin.
    //
    GPIO_setMasterCore(15, GPIO_CORE_CPU1);
    GPIO_setPinConfig(GPIO_15_SCIRXDB);
    GPIO_setDirectionMode(15, GPIO_DIR_MODE_OUT);
    GPIO_setPadConfig(15, GPIO_PIN_TYPE_STD);
    GPIO_setQualificationMode(15, GPIO_QUAL_ASYNC);

    //
    // Map the ISR to the wake interrupt.
    //
    Interrupt_register(INT_SCIB_RX, scibRxISR);

    //
    // Initialize SCIB and its FIFO.
    //
    SCI_performSoftwareReset(SCIB_BASE);

    //
    // Configure SCIB for echoback.
    //
    SCI_setConfig(SCIB_BASE, DEVICE_LSPCLK_FREQ, 9600, (SCI_CONFIG_WLEN_8 |
                                             SCI_CONFIG_STOP_ONE |
                                             SCI_CONFIG_PAR_NONE));
    SCI_resetChannels(SCIB_BASE);
    SCI_clearInterruptStatus(SCIB_BASE, SCI_INT_TXFF | SCI_INT_RXFF);
    SCI_enableFIFO(SCIB_BASE);
    SCI_enableModule(SCIB_BASE);
    SCI_performSoftwareReset(SCIB_BASE);

    //
    // Set the transmit FIFO level to 0 and the receive FIFO level to 10.
    // Enable the TXFF and RXFF interrupts.
    //
    SCI_setFIFOInterruptLevel(SCIB_BASE, SCI_FIFO_TX0, SCI_FIFO_RX10);
    SCI_enableInterrupt(SCIB_BASE, SCI_INT_RXFF);

    //
    // Clear the SCI interrupts before enabling them.
    //
    SCI_clearInterruptStatus(SCIB_BASE, SCI_INT_TXFF | SCI_INT_RXFF);

    //
    // Enable the interrupts in the PIE: Group 9 interrupts 1 & 2.
    //
    Interrupt_enable(INT_SCIB_RX);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP9);

}

void quad_sci_write(uint16_t * data, uint16_t size)
{
    SCI_writeCharArray(SCIB_BASE, data, size);
}


//
// SCIBRxISR - Read two characters from the RXBUF and echo them back.
//
__interrupt void
scibRxISR(void)
{
    CommandReceivedData[0] = SCI_readCharBlockingFIFO(SCIB_BASE);
    CommandReceivedData[1] = SCI_readCharBlockingFIFO(SCIB_BASE);
    CommandReceivedData[2] = SCI_readCharBlockingFIFO(SCIB_BASE);
    CommandReceivedData[3] = SCI_readCharBlockingFIFO(SCIB_BASE);
    CommandReceivedData[4] = SCI_readCharBlockingFIFO(SCIB_BASE);
    CommandReceivedData[5] = SCI_readCharBlockingFIFO(SCIB_BASE);
    CommandReceivedData[6] = SCI_readCharBlockingFIFO(SCIB_BASE);
    CommandReceivedData[7] = SCI_readCharBlockingFIFO(SCIB_BASE);
    CommandReceivedData[8] = SCI_readCharBlockingFIFO(SCIB_BASE);
    CommandReceivedData[9] = SCI_readCharBlockingFIFO(SCIB_BASE);

    //
    // Clear the SCI RXFF interrupt and acknowledge the PIE interrupt.
    //
    SCI_clearInterruptStatus(SCIB_BASE, SCI_INT_RXFF);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP9);

    NewCommandAvailable = 1;
}
